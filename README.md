# orchestra

This repository contains the way my servers are orchestrated.
This isn't intended to be the nicest way of doing things, but the fastest and good enough for me to have a reproducible deployment.

## Secrets

Secrets should be gitcrypt-encrypted here, and if I left something public, please ping me. 
